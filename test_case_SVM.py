from impl import *
def class_def(age):
	if age < 20:
		return 0;
	elif age < 35:
		return 1;
	elif age < 55:
		return 2;
	else:
		return 3;
		
def class_def2(age):
	if age < 20:
		return 0;
	elif age < 55:
		return 1;
	else:
		return 2;
		
print("Data Loading-----------------------------")
data_loader_200_class1 = LoadData("data", "wiki.csv", class_def, seed=3, read_first=200, face_size=(10,10))
data_loader_1000_class1 = LoadData("data", "wiki.csv", class_def, seed=3, read_first=1000, face_size=(10,10))
data_loader_12000_class1 = LoadData("data", "wiki.csv", class_def, seed=32323, read_first=4000, face_size=(10,10))
data_loader_200_class2 = LoadData("data", "wiki.csv", class_def2, seed=3, read_first=200, face_size=(10,10))
data_loader_1000_class2 = LoadData("data", "wiki.csv", class_def2, seed=3, read_first=1000, face_size=(10,10))
data_loader_12000_class2 = LoadData("data", "wiki.csv", class_def2, seed=32323, read_first=4000, face_size=(10,10))

print("Pre Processing-----------------------------")
preprocessing_lbp_nopca = PreProcessing(lbp=True, hist=True, pca=False)
preprocessing_gray_nopca = PreProcessing(lbp=False, hist=True, pca=False)

# Experiment Definitions
experiment1 = Experiment(data_loader_200_class1, preprocessing_gray_nopca, [Alg_SVM()])
experiment2 = Experiment(data_loader_1000_class1, preprocessing_gray_nopca, [Alg_SVM()])
experiment3 = Experiment(data_loader_12000_class1, preprocessing_gray_nopca, [Alg_SVM()])
experiment4 = Experiment(data_loader_200_class1, preprocessing_lbp_nopca, [Alg_SVM()])
experiment5 = Experiment(data_loader_1000_class1, preprocessing_lbp_nopca, [Alg_SVM()])
experiment6 = Experiment(data_loader_12000_class1, preprocessing_lbp_nopca, [Alg_SVM()])
experiment7 = Experiment(data_loader_200_class2, preprocessing_gray_nopca, [Alg_SVM()])
experiment8 = Experiment(data_loader_1000_class2, preprocessing_gray_nopca, [Alg_SVM()])
experiment9 = Experiment(data_loader_12000_class2, preprocessing_gray_nopca, [Alg_SVM()])
experiment10 = Experiment(data_loader_200_class2, preprocessing_lbp_nopca, [Alg_SVM()])
experiment11 = Experiment(data_loader_1000_class2, preprocessing_lbp_nopca, [Alg_SVM()])
experiment12 = Experiment(data_loader_12000_class2, preprocessing_lbp_nopca, [Alg_SVM()])

print("Experimenting-----------------------------")
# Run Experiments
experiment1.run()
experiment2.run()
experiment3.run()
experiment4.run()
experiment5.run()
experiment6.run()
experiment7.run()
experiment8.run()
experiment9.run()
experiment10.run()
experiment11.run()
experiment12.run()