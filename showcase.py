import cv2
import numpy as np
from impl import *

winName = "Age Detection"
cv2.namedWindow(winName)

#import required libraries 
import numpy as np
import cv2
import matplotlib.pyplot as plt
import time 

def class_def(age):
	if age < 20:
		return 0;
	elif age < 55:
		return 1;
	else:
		return 2;
		
print("Data Loading-----------------------------")
data_loader_100_class2 = LoadData("data", "wiki.csv", class_def, seed=3, read_first=200, face_size=(10,10))

print("Pre Processing-----------------------------")
preprocessing_gray_nopca = PreProcessing(lbp=False, hist=True, pca=False)

# Experiment Definitions
experiment7 = Experiment(data_loader_100_class2, preprocessing_gray_nopca, [Alg_NN_MLP(hidden_layer_sizes=(50,3), max_iter=100, alpha=1e-14, solver='sgd', verbose=False, tol=1e-14, learning_rate_init=.01)])

print("Experimenting-----------------------------")
# Run Experiments
experiment7.run()

model = experiment7.get_model_trained()

def detect_faces(f_cascade, colored_img, scaleFactor = 1.1):
    img_copy = np.copy(colored_img)
    #convert the test image to gray image as opencv face detector expects gray images
    gray = cv2.cvtColor(img_copy, cv2.COLOR_BGR2GRAY)
    
    #let's detect multiscale (some images may be closer to camera than others) images
    faces = f_cascade.detectMultiScale(gray, scaleFactor=scaleFactor, minNeighbors=5);
    if len(faces) < 1:
        return colored_img, colored_img
    (x, y, w, h) = faces[0]
    crop_img = img_copy[y:y+h, x:x+w]
   
    #go over list of faces and draw them as rectangles on original colored img
    for (x, y, w, h) in faces:
        cv2.rectangle(img_copy, (x, y), (x+w, y+h), (0, 255, 0), 2)
    return img_copy, crop_img

haar_face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
cam = cv2.VideoCapture(0)
while(cam.isOpened()): 
    s, im = cam.read() # captures image
    cv2.waitKey(10)
    im = np.uint8(im)
    faces_detected_img, cropped = detect_faces(haar_face_cascade, im)
    #conver image to RGB and show image
    cv2.imshow(winName, faces_detected_img)
    image = np.array(cropped)
    preprocessing_gray_nopca.set_data([[0,0,0,0,0,0,image]])
    preprocessing_gray_nopca.run()
    print("Class::" + str(model.predict(preprocessing_gray_nopca.get_data()[0])))
    