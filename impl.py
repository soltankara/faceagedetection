import scipy.io
import numpy as np
import datetime as dt
import csv
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from skimage.feature import local_binary_pattern
from skimage import io
from skimage.transform import rescale, resize, downscale_local_mean
import math
from sklearn.decomposition import PCA
from sklearn.svm import SVC
#from sklearn.preprocessing import Scaler
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
from sklearn.neural_network import MLPClassifier
import random

class LoadData():
	def __init__(self, data_folder, csv_file, age_classification, seed=1, read_first=8000, face_size=(20,20)):
		self.__data_folder = data_folder
		self.__csv_file = csv_file
		self.__read_first = read_first
		self.__face_size = face_size
		self.__age_classification = age_classification
		self.__seed = seed
		self.load_meta()
		self.load_images()

	def load_meta(self):
		random.seed(self.__seed)
		# Read CSV to load meta information
		self.__data = []
		with open(self.__data_folder + "/" + self.__csv_file,'rU') as csv_:
			readCSV = csv.reader(csv_, delimiter=';')
			for row in random.sample([r for r in readCSV], self.__read_first):
				row[-1] = self.__age_classification(int(row[-1]))
				self.__data.append(row)

	def load_images(self):
		# Image reading and preprocessing
		for i in range(len(self.__data)):
			# read image to grayscale
			gray = io.imread(self.__data_folder + "/imgs/" + self.__data[i][0], True)
			# facecuts
			gray_cutted = gray[int(float(self.__data[i][2])):int(float(self.__data[i][4])),int(float(self.__data[i][1])):int(float(self.__data[i][3]))]
			# resize to fixed images
			gray_resized = resize(gray_cutted, self.__face_size)
			# Add data to data array
			self.__data[i].append(gray_resized)
			if i % 100 == 0:
				print("Read image::" + str(i) + "/" + str(len(self.__data)))
				
	def get_data(self):
		return self.__data

class PreProcessing():
	def __init__(self, lbp=True, hist=True, pca=True, lbp_radius=3, lbp_n_points_factor=8, lbp_m='ror', n_components = 50):
		self.__data = None
		self.__lbp_radius = lbp_radius
		self.__lbp_n_points_factor = lbp_n_points_factor
		self.__lbp_m = lbp_m
		self.__n_components = n_components
		self.__lbp = lbp
		self.__hist = hist
		self.__pca = pca
		
	def lbp(self):
		
		# Parameters for LBP.
		lbp_n_points = self.__lbp_n_points_factor * self.__lbp_radius

		# Image reading and preprocessing
		for i in range(len(self.__data[0])):
			gray_resized = self.__data[0][i]
			# LBP
			lbp = np.array(local_binary_pattern(gray_resized,lbp_n_points, self.__lbp_radius, self.__lbp_m))
			# Add data to data array
			self.__data[0][i] = lbp
			if i % 100 == 0:
				print("Calc LBP::" + str(i) + "/" + str(len(self.__data[0])))

	def hist(self):
		
		# preprocessing
		for i in range(len(self.__data[0])):
			img = self.__data[0][i]
			# Histogram
			hist = np.histogram(img, list(range(255)))[0]
			# Add data to data array
			self.__data[0][i] = hist
			if i % 100 == 0:
				print("Calc Histogram::" + str(i) + "/" + str(len(self.__data[0])))

	def pca(self):
		
		pca = PCA(n_components=self.__n_components, svd_solver='randomized', whiten=True).fit(self.__data[0])
		self.__data = (pca.transform(self.__data[0]), self.__data[1])
	
	def run(self):
		if self.__lbp:
			self.lbp()
		if self.__hist:
			self.hist()
		else:
			self.__data = ([data.flatten() for data in self.__data[0]], self.__data[1]) 
		if self.__pca:
			self.pca()
			
	def get_data(self):
		return self.__data
	
	def set_data(self, data):
		self.__data = data
		self.__data = ([item[6] for item in self.__data], [item[5] for item in self.__data])
		
class Alg_SVM():
	def __init__(self, cs = [0.001, 0.01, 0.1, 1, 10], gammas = [0.001, 0.01, 0.1, 1, 10], nfolds=4):
		self.__cs = cs
		self.__gammas = gammas
		self.__model = None
		self.__nfolds = nfolds
		
	def run(self):
		#http://scikit-learn.org/stable/modules/cross_validation.html
		# Gridsearch hyper parameter tuning
		from sklearn import svm, grid_search
		def svc_param_selection(X, y, nfolds):
			param_grid = {'C': self.__cs, 'gamma' : self.__gammas}
			grid_search = GridSearchCV(svm.SVC(decision_function_shape='ovo', degree=3, kernel='rbf'), param_grid, cv=nfolds)
			grid_search.fit(X, y)
			grid_search.best_params_
			return grid_search.best_params_

		params = svc_param_selection(self.__X_train,self.__Y_train, self.__nfolds)
		print("The best classifier is: ", params)

		# SVM model training
		self.__model = SVC(C=params["C"], decision_function_shape='ovo', degree=3, gamma=params["gamma"], kernel='rbf')
		self.__model.fit(self.__X_train,self.__Y_train)
		
	def get_model(self):
		return self.__model
		
	def score(self):
		return (self.__model.score(self.__X_train, self.__Y_train), self.__model.score(self.__X_test, self.__Y_test))

	def set_data(self, data):
		self.__X_train = data["X_train"]
		self.__X_test = data["X_test"]
		self.__Y_train = data["Y_train"]
		self.__Y_test = data["Y_test"]

class Alg_NN_MLP():
	def __init__(self, hidden_layer_sizes=(50,2), max_iter=100, alpha=1e-14, solver='sgd', verbose=10, tol=1e-14, random_state=1, learning_rate_init=.001):
		self.__hidden_layer_sizes = hidden_layer_sizes
		self.__max_iter = max_iter
		self.__alpha = alpha
		self.__solver = solver
		self.__verbose = verbose
		self.__tol = tol
		self.__random_state = random_state
		self.__learning_rate_init = learning_rate_init
		
	def run(self):
		# MLP_NN model training
		self.__model = MLPClassifier(hidden_layer_sizes=self.__hidden_layer_sizes, max_iter=self.__max_iter, alpha=self.__alpha, solver=self.__solver, verbose=self.__verbose, tol=self.__tol, random_state=self.__random_state, learning_rate_init=self.__learning_rate_init)
		self.__model.fit(self.__X_train,self.__Y_train)
		
	def get_model(self):
		return self.__model
		
	def score(self):
		return (self.__model.score(self.__X_train, self.__Y_train), self.__model.score(self.__X_test, self.__Y_test))

	def set_data(self, data):
		self.__X_train = data["X_train"]
		self.__X_test = data["X_test"]
		self.__Y_train = data["Y_train"]
		self.__Y_test = data["Y_test"]

class Experiment():
	def __init__(self, data_loader, preprocessing, algorithms):
		preprocessing.set_data(data_loader.get_data())
		preprocessing.run()
		self.__data = preprocessing.get_data()
		self.__algorithms = algorithms
		
	def run(self, train_test_ratio=0.8):
		data = {}
		data["X_train"], data["Y_train"] = [item for item in self.__data[0][0:int(len(self.__data[0])*train_test_ratio)]], [item for item in self.__data[1][0:int(len(self.__data[0])*train_test_ratio)]]
		data["X_test"], data["Y_test"] = [item for item in self.__data[0][int(len(self.__data[0])*train_test_ratio):-1]], [item for item in self.__data[1][int(len(self.__data[0])*train_test_ratio):-1]]
		for alg in self.__algorithms:
			alg.set_data(data)
			alg.run()
			print(alg.score())
	def get_model_trained(self):
		return self.__algorithms[0].get_model()