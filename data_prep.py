import scipy.io
import numpy as np
import datetime as dt
import csv
import math

data = scipy.io.loadmat("wiki.mat")

full_path = data['wiki'][0,0]['full_path'][0]
face_location = data['wiki'][0,0]['face_location'][0]
photo_taken = data['wiki'][0,0]['photo_taken'][0]
dob = data['wiki'][0,0]['dob'][0]

writer = csv.writer(open("wiki.csv", 'w'), delimiter=';')
for item in range(0,len(data['wiki'][0,0]['dob'][0])-1):
	age = photo_taken[item] - (dt.datetime.fromordinal(int(dob[item])) + dt.timedelta(days=int(dob[item])%1) - dt.timedelta(days = 366)).year
	if age < 5 or (face_location[item][0][0] == 1 and face_location[item][0][1] == 1) or (abs(face_location[item][0][2] - face_location[item][0][0]) < 20) or (abs(face_location[item][0][3] - face_location[item][0][1]) < 20):
		continue
	writer.writerow([full_path[item][0], face_location[item][0][0], face_location[item][0][1], face_location[item][0][2], face_location[item][0][3], age])